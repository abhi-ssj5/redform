//
//  commonFun.swift
//  form-red
//
//  Created by Sierra 4 on 06/02/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import Foundation
import UIKit

class alert {
    class func displayAlert(_ userMessage: String,_ obj:UIViewController) {
        let alertMessage = UIAlertController(title:"Alert", message: userMessage, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title:"OK", style: UIAlertActionStyle.default, handler: nil)
        alertMessage.addAction(okAction)
        obj.present(alertMessage, animated: true, completion: nil)
    }
}
