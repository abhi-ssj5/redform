//
//  ViewController.swift
//  form-red
//
//  Created by Sierra 4 on 03/02/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var btnFaceBook: UIButton!
    @IBOutlet weak var btnMoreOptions: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnSignUp: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnFaceBook.layer.cornerRadius = 25
        btnLogin.layer.cornerRadius = 25
        btnSignUp.layer.cornerRadius = 25
        btnSignUp.layer.borderWidth = 2
        btnSignUp.layer.borderColor = UIColor.white.cgColor
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func signInFacebook(_ sender: Any) {
        performSegue(withIdentifier: "loginPage", sender: nil)
    }
    
    @IBAction func loginClicked(_ sender: Any) {
        performSegue(withIdentifier: "loginPage", sender: nil)
        
    }
    
    @IBAction func signUpClicked(_ sender: Any) {
        performSegue(withIdentifier: "signUpPage", sender: nil)
    }
    
}

