//
//  ViewControllerSignUp.swift
//  form-red
//
//  Created by Sierra 4 on 03/02/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import UIKit

class ViewControllerSignUp: UIViewController, UITextFieldDelegate {
    
   
    @IBOutlet weak var btnGoBackHome: UIButton!
    @IBOutlet weak var btnSignUpStepTwo: UIButton!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtpswd: UITextField!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return false
    }
    
    @IBAction func goBackToHomeClicked(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
        
    }
    
    /*@IBAction func signinClicked(_ sender: Any) {
        performSegue(withIdentifier: "signUpToLogin", sender: nil)
    }*/
    
    @IBAction func btnSignUpStepTwoClicked(_ sender: Any) {
        var details = [String: String]()
        details["email"] = txtEmail.text!
        details["pswd"] = txtpswd.text!
        
        if validateEmail(details["email"]!) && validatePswd(details["pswd"]!)
        {
            performSegue(withIdentifier: "goToSignUpTwo", sender: details)
        }
        else
        {
            alert.displayAlert("Invalid email or password!", self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "goToSignUpTwo") {
            let DestViewController = segue.destination as! ViewControllerSignUpStepTwo
            DestViewController.usrDetail = sender as! [String: String]
        }
    }
    
    func validateEmail(_ email: String) -> Bool
    {
        if email.isEmpty {
            return false
        }
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        if !emailTest.evaluate(with: email) {
            return false
        }
        return true
    }
    
    
    func validatePswd(_ pswd: String) -> Bool
    {
        if pswd.isEmpty {
            return false
        }
        return true
    }
}
