//
//  ViewControllerSignUpComplete.swift
//  form-red
//
//  Created by Sierra 4 on 06/02/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import UIKit

class ViewControllerSignUpComplete: UIViewController {

    @IBOutlet weak var lblDetails: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    
    var details = [String: String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblDetails.text = ""
        for (key,value) in details
        {
            lblDetails.text = lblDetails.text! + "\n" + key + " : " + value
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backClicked(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }

   

}
