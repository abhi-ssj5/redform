//
//  ViewControllerLoginSuccess.swift
//  form-red
//
//  Created by Sierra 4 on 04/02/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import UIKit

class ViewControllerLoginSuccess: UIViewController {
    
    @IBOutlet weak var backToLogin: UIButton!
    @IBOutlet weak var lblWelcome: UILabel!
    var usrEmail: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        lblWelcome.text = "Welcome " + usrEmail
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backToLoginClicked(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
        //performSegue(withIdentifier: "successToLogin", sender: nil)
    }

   }
