//
//  ViewControllerLogin.swift
//  form-red
//
//  Created by Sierra 4 on 03/02/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import UIKit

class ViewControllerLogin: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var goBackHome: UIButton!
    @IBOutlet weak var loginNext: UIButton!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return false
    }
    
    @IBAction func goBackClicked(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func loginNextClicked(_ sender: Any) {
        let email: String = txtEmail.text!
        let pswd: String = txtPassword.text!
        
        
        if validateEmail(email) && validatePswd(pswd)
        {
            performSegue(withIdentifier: "loginToSuccess", sender: email)
        }
        else
        {
            alert.displayAlert("Invalid email or password!",self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let DestViewController = segue.destination as! ViewControllerLoginSuccess
        DestViewController.usrEmail = sender as! String
    }
    
    func validateEmail(_ email: String) -> Bool
    {
        if email.isEmpty {
            return false
        }
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        if !emailTest.evaluate(with: email) {
            return false
        }
        return true
    }
    
    
    func validatePswd(_ pswd: String) -> Bool
    {
        if pswd.isEmpty {
            return false
        }
        return true
    }
    
}
