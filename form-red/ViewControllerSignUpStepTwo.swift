//
//  ViewControllerSignUpStepTwo.swift
//  form-red
//
//  Created by Sierra 4 on 04/02/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import UIKit

class ViewControllerSignUpStepTwo: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate {

    @IBOutlet weak var finishSignUp: UIButton!
    @IBOutlet weak var backToSignUp: UIButton!
    @IBOutlet weak var txtFName: UITextField!
    @IBOutlet weak var txtLName: UITextField!
    @IBOutlet weak var imgPick: UIImageView!
    @IBOutlet weak var btnImgPicker: UIButton!
    
    let imagePicker = UIImagePickerController()
    
    var usrDetail = [String: String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imgPick.layer.cornerRadius = 42
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return false
    }
    
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    @IBAction func imagePickerClicked(_ sender: Any) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        present(imagePicker, animated: true, completion: nil)
    }

    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        imgPick.contentMode = .scaleAspectFit
        imgPick.image = chosenImage
        dismiss(animated:true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func backToSignUpClicked(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func finishClicked(_ sender: Any) {
        usrDetail["First Name"] = txtFName.text
        usrDetail["Last Name"] = txtLName.text
        
        if checkName(value: usrDetail["First Name"]!, field: "First Name") && checkName(value: usrDetail["Last Name"]!, field: "Last Name")
        {
            performSegue(withIdentifier: "signUpComplete", sender: usrDetail)
        }
        else
        {
            return
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let DestViewController = segue.destination as! ViewControllerSignUpComplete
        DestViewController.details = sender as! [String: String]
    }
    
    func checkName(value: String, field: String) -> Bool
    {
        if value.isEmpty {
            alert.displayAlert("Please enter \(field) ", self)
            return false
        }
        let letters = CharacterSet.letters
        if value.characters.count > 25 || value.characters.count < 5 {
            alert.displayAlert("\(field) exceeding character limit", self)
            return false
        }
        for char in value.unicodeScalars {
            if !letters.contains(char) {
                alert.displayAlert("Invalid \(field)", self)
                return false
            }
        }
        return true
    }
}
